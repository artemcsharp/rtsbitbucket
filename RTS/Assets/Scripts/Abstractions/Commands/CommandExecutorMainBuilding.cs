﻿using UnityEngine;

namespace Abstractions.Commands
{
    public abstract class CommandExecutorMainBuilding<T> : CommandExecutorBase, ICommandExecutor where T : ICommand
    {

    }
}