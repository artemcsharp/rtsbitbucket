using Abstractions.Commands;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CommandExecutorBase : MonoBehaviour, ICommandExecutor
{
    public void ExecuteCommand(object command) => ExecuteSpecificCommand(command);

    public abstract void ExecuteSpecificCommand(object command);
}
