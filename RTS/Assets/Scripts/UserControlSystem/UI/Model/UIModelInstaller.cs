using UnityEngine;
using Zenject;

public class UIModelInstaller : MonoInstaller
{
    [SerializeField] private GameObject _unitPrefab;

    public override void InstallBindings()
    {
        Container.Bind<GameObject>().WithId("Chomper").FromInstance(_unitPrefab).AsSingle();
    }
}