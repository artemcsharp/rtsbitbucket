using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Abstractions;
using UnityEngine.EventSystems;
using UserControlSystem;
using QuickOutline;

public class OutLinePresenter : MonoBehaviour
{
    [SerializeField] private Camera _camera;

    private Outline _currentOutline;

    void Update()
    {

        var hits = Physics.RaycastAll(_camera.ScreenPointToRay(Input.mousePosition));
        if (hits.Length == 0)
        {
            return;
        }

        var outline = hits
        .Select(hit => hit.collider.GetComponentInParent<Outline>())
        .FirstOrDefault(c => c != null);

        if (outline != null)
        {
            _currentOutline = outline;
            _currentOutline.enabled = true;
        }
        else if (_currentOutline != null)
        {
            _currentOutline.enabled = false;
            _currentOutline = null;
        }
    }
}
