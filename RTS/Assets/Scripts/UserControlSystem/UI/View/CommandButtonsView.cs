﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abstractions.Commands;
using Abstractions.Commands.CommandsInterfaces;
using UnityEngine;
using UnityEngine.UI;

namespace UserControlSystem.UI.View
{
    public sealed class CommandButtonsView : MonoBehaviour
    {
        public Action<ICommandExecutor, Type> OnClick;

        [SerializeField] private GameObject _attackButton;
        [SerializeField] private GameObject _moveButton;
        [SerializeField] private GameObject _patrolButton;
        [SerializeField] private GameObject _stopButton;
        [SerializeField] private GameObject _produceUnitButton;

        private Dictionary<Type, GameObject> _buttonsByExecutorType;

        private void Start()
        {
            
            _buttonsByExecutorType = new Dictionary<Type, GameObject>();
            _buttonsByExecutorType
                .Add(typeof(IAttackCommand), _attackButton);
            _buttonsByExecutorType
                .Add(typeof(IMoveCommand), _moveButton);
            _buttonsByExecutorType
                .Add(typeof(IPatrolCommand), _patrolButton);
            _buttonsByExecutorType
                .Add(typeof(IStopCommand), _stopButton);
            _buttonsByExecutorType
                .Add(typeof(IProduceUnitCommand), _produceUnitButton);

        }

        public void BlockInteractions(ICommandExecutor ce)
        {
            UnblockAllInteractions();
            getButtonGameObjectByType(ce.GetType())
            .GetComponent<Selectable>().interactable = false;
        }

        public void UnblockAllInteractions() => setInteractible(true);

        private void setInteractible(bool value)
        {
            _attackButton.GetComponent<Selectable>().interactable = value;
            _moveButton.GetComponent<Selectable>().interactable = value;
            _patrolButton.GetComponent<Selectable>().interactable = value;
            _stopButton.GetComponent<Selectable>().interactable = value;
            _produceUnitButton.GetComponent<Selectable>().interactable =
            value;
        }

        public void MakeLayout(ICommandExecutor commandExecutor)
        {
            var types = commandExecutor.GetType().BaseType.GetGenericArguments();
            foreach(var currentExecutor in types)
            {
                var buttonGameObject = _buttonsByExecutorType
                    .First(type => type
                    .Key
                    .IsEquivalentTo(currentExecutor))
                    .Value;
                MakeElementLayout(commandExecutor, buttonGameObject, currentExecutor);
            }
        }
        private void MakeElementLayout(ICommandExecutor commandExecutors, GameObject button, Type typeOfCommand)
        {
            button.gameObject.SetActive(true);
            var buttonComponent = button.GetComponent<Button>();
            buttonComponent.onClick.AddListener(() => OnClick?.Invoke(commandExecutors, typeOfCommand));
        }

        private GameObject getButtonGameObjectByType(Type executorInstanceType)
        {
            return _buttonsByExecutorType
            .Where(type =>
            type.Key.IsAssignableFrom(executorInstanceType))
            .First()
            .Value;
        }




        public void Clear()
        {
            foreach (var kvp in _buttonsByExecutorType)
            {
                kvp.Value.GetComponent<Button>().onClick.RemoveAllListeners();
                kvp.Value.SetActive(false);
            }
        }
    }
}