using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Abstractions;
using Abstractions.Commands;
using Abstractions.Commands.CommandsInterfaces;
using System;

public class MainUnit : CommandExecutorUnit<IAttackCommand, IPatrolCommand, IStopCommand, IMoveCommand>, ISelectable
{
    public float Health => _health;
    public float MaxHealth => _maxHealth;
    public Sprite Icon => _icon;

    [SerializeField] private Transform _unitsParent;

    [SerializeField] private float _maxHealth = 1000;
    [SerializeField] private Sprite _icon;

    private float _health = 1000;

    private Dictionary<Type, Action> _ActionsByExecutorType;

    private void Start()
    {
        _ActionsByExecutorType = new Dictionary<Type, Action>();
        _ActionsByExecutorType
            .Add(typeof(IAttackCommand), () => { Debug.Log("Attack"); });
        _ActionsByExecutorType
            .Add(typeof(IMoveCommand), () => { Debug.Log("Move"); });
        _ActionsByExecutorType
            .Add(typeof(IPatrolCommand), () => { Debug.Log("Patrol"); });
        _ActionsByExecutorType
            .Add(typeof(IStopCommand), () => { Debug.Log("Stop"); });
    }

    public override void ExecuteSpecificCommand(object command)
    {
        _ActionsByExecutorType[command as Type].Invoke();
    }
}
